const fs = require("fs");

// console.log(fs);
// bát đồng bộ
fs.readFile('./docs/blog1.txt', (err, data) => {
    if (err) {
        console.log(err);
    }
    console.log(data.toString());
});



fs.writeFile('./docs/blog1.txt', 'Heeelllo , i`m Kha ', () => {
    console.log('file was written');
})
console.log("last line");
fs.writeFile('./docs/blog2.txt', 'now i`m create a new file txt ', () => {
    console.log('file was create');
})

// directories 
if (!fs.existsSync('./assets')) {
    fs.mkdir('./assets', (err) => {
        if (err) {
            console.log(err);
        }
        console.log('folder created');
    })
} else {
    fs.rmdir('./assets', (err) => {
        if (err) {
            console.log(err);
        }
        console.log('folder deleted');
    })
}
// deleting files 
if (fs.existsSync('./docs/deleteme.txt')) {
    fs.unlink('./docs/deleteme.txt', (err) => {
        if (err) {
            console.log(err);
        }
        console.log('file deleted');
    })
}
